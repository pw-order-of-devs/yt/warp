#[macro_use] extern crate serde_derive;

use std::env;
use std::collections::HashMap;
use warp::{Filter, filters::BoxedFilter, http::Response, Reply};

#[derive(Deserialize, Serialize)]
pub struct HelloQuery {
    name: String,
}

#[derive(Deserialize, Serialize)]
pub struct HelloRequest {
    name: String,
}

fn version() -> BoxedFilter<(impl Reply,)> {
    warp::path!("version").map(|| "0.1.0").boxed()
}

fn version_json() -> BoxedFilter<(impl Reply,)> {
    warp::path!("version" / "json").map(|| warp::reply::json(&format!("0.1.0"))).boxed()
}

fn hello_query() -> BoxedFilter<(impl Reply,)> {
    warp::path!("hello")
        .and(warp::query::<HashMap<String, String>>())
        .map(|p: HashMap<String, String>| match p.get("name") {
            Some(name) => Response::builder().body(format!("{}", name)),
            None => Response::builder().status(400).body(format!("Key \"name\" is missing")),
        })
        .boxed()
}

fn hello_query_2() -> BoxedFilter<(impl Reply,)> {
    warp::path!("hello" / "query")
        .and(warp::query::<HelloQuery>())
        .map(|p: HelloQuery| warp::reply::json(&format!("Hello, {}", p.name)))
        .boxed()
}

fn hello_path() -> BoxedFilter<(impl Reply,)> {
    warp::path!("hello" / "path" / String)
        .map(|name| warp::reply::json(&format!("Hello, {}", name)))
        .boxed()
}

fn hello_path_2() -> BoxedFilter<(impl Reply,)> {
    warp::path!("hello" / "path" / "alt")
        .and(warp::path::param::<String>())
        .map(|name| warp::reply::json(&format!("Hello, {}", name)))
        .boxed()
}

fn header_test() -> BoxedFilter<(impl Reply,)> {
    warp::path!("header" / "test")
        .and(warp::header("authorization").map(|token: String| { token }))
        .map(|token| format!("{}", token))
        .boxed()
}

fn gets() -> BoxedFilter<(impl Reply,)> {
    warp::get()
        .and(version()
            .or(version_json())
            .or(hello_query())
            .or(hello_query_2())
            .or(hello_path())
            .or(hello_path_2())
            .or(header_test())
        )
        .boxed()
}

fn hello_json() -> BoxedFilter<(impl Reply,)> {
    warp::path!("hello" / "json")
        .and(warp::body::json())
        .map(|body: HelloRequest| warp::reply::json(&format!("Hello, {}", body.name)))
        .boxed()
}

fn posts() -> BoxedFilter<(impl Reply,)> {
    warp::post()
        .and(hello_json())
        .boxed()
}

fn routes() -> BoxedFilter<(impl Reply,)> {
    warp::path("api")
        .and(gets().or(posts()))
        .with(warp::log("something's going on"))
        .boxed()
}

#[tokio::main]
async fn main() {
    if env::var_os("RUST_LOG").is_none() { env::set_var("RUST_LOG", "info"); }
    pretty_env_logger::init();

    warp::serve(routes())
        .run(([127, 0, 0, 1], 3030))
        .await;
}